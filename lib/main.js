require("sdk/tabs").on("ready", logURL);
var ss = require("sdk/simple-storage");
var { ToggleButton } = require('sdk/ui/button/toggle');
var panels = require("sdk/panel");
var self = require("sdk/self");
var url = require("sdk/url");

if (!ss.storage.total) {
    ss.storage.total = 0;
}

if (!ss.storage.today) {
    ss.storage.today = {};
    date = getDate();
    ss.storage.today["date"] = date.valueOf();
    ss.storage.today["count"] = 0;
}

if (!ss.storage.pages) {
    ss.storage.pages = {};
}

exports.main = function (options, callbacks)
{
    panel.port.emit("totalcount",ss.storage.total);
    panel.port.emit("todaycount",ss.storage.today["count"]);
    panel.port.emit("thatsiteurl","...");
    panel.port.emit("thatsitecount",0);
}

function logURL(tab) {
    var u = url.URL(tab.url).host;
    var key = u;
    if (!(key in ss.storage.pages)) {
        ss.storage.pages[key] = 1;
    }
    else
    {
        ss.storage.pages[key]++;        
    }    
    panel.port.emit("thatsiteurl",u);
    panel.port.emit("thatsitecount",ss.storage.pages[key]);
    
    ///////////////////////////////////////////////////////
    
    date = getDate();
    if (ss.storage.today["date"] != date.valueOf()) {	
        ss.storage.today["count"] = 1;
        ss.storage.today["date"] = date.valueOf();
    }
    else
    {
       ss.storage.today["count"]++; 
    }
    panel.port.emit("todaycount",ss.storage.today["count"]);    
    ///////////////////////////////////////////////////////
    ss.storage.total++;
    panel.port.emit("totalcount",ss.storage.total);
    }

function getDate()
{
    date = new Date();
    dd = date.getDate();
    mm = date.getMonth() + 1;
    yy = date.getFullYear();
    date = new Date(yy,mm,dd);
    return date;
}

var button = ToggleButton({
  id: "my-button",
  label: "my button",
  icon: {
    "16": "./icon-16.png",
    "32": "./icon-32.png",
    "64": "./icon-64.png"
  },
  onChange: handleChange
});

var panel = panels.Panel({
  contentURL: self.data.url("panel.html"),
  contentScriptFile: [
		    self.data.url('jquery-2.1.1.min.js'),
		    self.data.url('panel.js')
],
  onHide: handleHide,
  height: 150,
  width: 450
});

function handleChange(state) {
  if (state.checked) {
    panel.show({
      position: button
    });
  }
}

function handleHide() {
  button.state('window', {checked: false});
}